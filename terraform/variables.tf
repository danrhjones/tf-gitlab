variable "gitlab_group_paths" {
  type        = map(string)
  description = "A map of GitLab groups to create projects in"
}

variable "gitlab_token" {
  type        = string
  description = "GitLab Token"
}

variable "share_group_id" {
  type        = string
  default     = "63249993"
  description = "The ID of the group that will be owners on the DPS platform owned repo"
}
