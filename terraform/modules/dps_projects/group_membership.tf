resource "gitlab_group_share_group" "test" {
  share_group_id = var.share_group_id
  group_id       = var.namespace_id
  group_access   = var.group_access
}
