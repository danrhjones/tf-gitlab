variable "namespace_id" {
  type        = string
  description = "The group to create projects under"
}
variable "project_name" {}

variable "default_settings" {
  type        = map(string)
  description = "Default settings for projects"
  default = {
    "merge_requests_enabled"                           = true
    "visibility_level"                                 = "public"
    "default_branch"                                   = "main"
    "initialize_with_readme"                           = true
    "request_access_enabled"                           = false
    "merge_method"                                     = "merge"
    "approvals_before_merge"                           = 1
    "only_allow_merge_if_pipeline_succeeds"            = true
    "only_allow_merge_if_all_discussions_are_resolved" = true
    "remove_source_branch_after_merge"                 = true
    "reset_approvals_on_push"                          = true
    "disable_overriding_approvers_per_merge_request"   = true
    "merge_requests_author_approval"                   = false
    "merge_requests_disable_committers_approval"       = true
  }
}

variable "share_group_id" {
  type        = string
  description = "ID of the group to be given permissions on a project"
}
variable "group_access" {
  type        = string
  description = "value"
  validation {
    condition     = contains(["no one", "minimal", "guest", "reporter", "developer", "maintainer", "owner", "master"], var.group_access)
    error_message = "Valid values for var: group_access are (no one, minimal, guest, reporter, developer, maintainer, owner, master)."
  }
}
