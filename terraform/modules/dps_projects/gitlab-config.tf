resource "gitlab_project" "gitlab-config" {
  namespace_id = var.namespace_id
  name         = "gitlab-config"
  description  = "${var.project_name} gitlab-config repo"

  visibility_level                                 = var.default_settings["visibility_level"]
  default_branch                                   = var.default_settings["default_branch"]
  initialize_with_readme                           = var.default_settings["initialize_with_readme"]
  request_access_enabled                           = var.default_settings["request_access_enabled"]
  merge_method                                     = var.default_settings["merge_method"]
  approvals_before_merge                           = var.default_settings["approvals_before_merge"]
  only_allow_merge_if_pipeline_succeeds            = var.default_settings["only_allow_merge_if_pipeline_succeeds"]
  only_allow_merge_if_all_discussions_are_resolved = var.default_settings["only_allow_merge_if_all_discussions_are_resolved"]
  remove_source_branch_after_merge                 = var.default_settings["remove_source_branch_after_merge"]
}

resource "gitlab_branch_protection" "gitlab_config_branch_protection" {
  project           = gitlab_project.gitlab-config.id
  branch            = var.default_settings["default_branch"]
  push_access_level = "no one"
}

resource "gitlab_project_level_mr_approvals" "gitlab_config_mr_approvals" {
  project_id                                     = gitlab_project.gitlab-config.id
  reset_approvals_on_push                        = var.default_settings["reset_approvals_on_push"]
  disable_overriding_approvers_per_merge_request = var.default_settings["disable_overriding_approvers_per_merge_request"]
  merge_requests_author_approval                 = var.default_settings["merge_requests_author_approval"]
  merge_requests_disable_committers_approval     = var.default_settings["merge_requests_disable_committers_approval"]
}
