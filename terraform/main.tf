data "gitlab_group" "this" {
  for_each  = var.gitlab_group_paths
  full_path = each.value
}

module "dapm_projects" {
  for_each = data.gitlab_group.this
  source   = "./modules/dps_projects"

  namespace_id   = each.value.group_id
  project_name   = each.value.name
  share_group_id = var.share_group_id
  group_access   = "guest"
}

